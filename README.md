# ProjetInformatique

<p> Ce projet informatique écrit en java développe une application bureautique destinée à une entreprise de gestion de biens immobiliers. 

Elle gère les interactions entre les clients de l'agence, les particuliers et les agents eux-mêmes. <p>

## Installation

Pour pouvoir excecuter le code, il faut suivre les étapes suivantes :

1. Dézipper le fichier contenant le code et la base de données appelé "Projet"
2. Copier le projet "ProjetInformatique" et la BdD "donneesagence.backup" dans un dossier eclipse-workspace
3. Sur internet, telecharger "postgresql-42.2.5.jar" et le copier au même endroit
4. Lancer eclipse
5. Aller dans File -> Open Projects from File System -> Directory -> Selectionner le projet "ProjetInformatique"
6. Clic droit sur le projet -> Build Path -> Configure Build Path -> Libraries -> ClassPath -> Add External JARs -> Sectionner "postgresql-42.2.5.jar"
7. Ouvrir pgAdmin III et ajouter une base de données en local nommée "donneesagence"
8. Clic droit sur la base de données -> Restaurer -> Nom du fichier -> Selectionner "donneesagence.backup"
9. Le projet est prêt à l'execution !

## Utilisation et exemples

Notre code permet d'ouvrir une interface !

Pour l'ouvrir, il faut exectuter la classe MainApplication du package "application".

- Une première fenêtre s'ouvre. 
    Vous pouvez choisir entre : 
    1. Etre un client de l'agence qui cherche à vendre ou louer son bien (cliquez sur « Client »).
    2. Etre un particulier à la recherche d'un bien (cliquez sur « Public »).
    3. Etre un agent qui gère ses clients (cliquez sur « Agent »).
    4. Etre un responsable d'agence gère ses client et son agence (cliquez sur « Responsable »).


- Si vous avez cliqué sur Client : 


    Vous pourrez : 
    1. Créer un compte (cliquez sur « Creer un compte »).
        Dans ce cas, on demande votre nom, prenom, telephone, mail et un mot de passe.
        Un identifiant vous est attribué (gardez précieusuement cet identifiant et votre mot de passe).
    
    2. Vous connecter à l'application (cliquez sur « Se connecter »).
        Soit en entrant votre id et mot de passe précédemment crées, ou en entrant les données du client LERAULT Arthur id = 1 et mot de passe = arthur-15.
        Vous pouvez alors :
    
        1. Rédiger une annonce (cliquez sur « Rédiger une annonce »).
            Une fenetre vous permet de choisir entre une Maison, un Appartement, un Local, un Chateau, un Parking ou un Terrain.
            Cliquez sur le type de bien que vous souhaitez déposer et remplissez les caractéristiques.
            Votre bien est alors déposé à l'agence.
            NB : si vous trouvez que l'adresse est trop longue a rentrer, tapez "0/0/0/0/0/0" 6 zéros.
            
        2. Demander une estimation de votre bien (cliquez sur « Demander une estimation »).
            Une fenêtre vous donne un prix établi par un agent aléatoire.
            Si le prix vous convient (cliquez sur « Oui »), le bien est publié par l'agent en question.
            Sinon (cliquez sur « Non »), votre demande ne prendra pas suite.
            
        3. Classer les candidats interesses par votre bien (cliquez sur « Classer les candidats »).
            Une fenêtre vous permet d'entrer votre id (celui qui vous avez gardé ou 1 si vous êtes LERAULT Arthur).
            Vous devez aussi enter l'id du bien qui vous interesse. Si vous avez rédigé une annonce, entrez le numéro de ce bien, sinon entrez 1 (c'est le bien du client LERAULT Arthur).
            Les particuliers interessés par votre bien apparaitront : vous pouvez les classer.



- Si vous avez cliqué sur Public : 


    Vous pourrez : 
    1. Rechercher un bien (cliquez sur « Rechercher un bien »).
        Il vous suffit de choisir le type de bien, le type de transaction, la surface minimale et votre budget maximal.
        Les biens qui correspondent s'afficheront dans la console.
    
    2. Rechercher un bien de façon plus poussée (cliquez sur « Entrer plus de critères »).
        Vous pouvez choisir le type de bien que vous voulez et choisir de nombreux critères supplémentaires.
        Tous les critères sont facultatifs et ont une valeur par défaut qui séléctionne tous les biens.
        Par exemple, cliquer sur "terrasse necessaire", ne séléctionne que les biens ayant une terrasse (le bouton devient vert).
        Mais ne pas cliquer sur "terrasse" séléctionne tous les biens qu'ils aient une terrasse ou non.
        Une fois que vous avez entré vos critères principaux, cliquez sur "Lancer la recherche".
        Les biens qui correspondent s'afficheront dans la console.
    
    3. Fixer un rendez-vous de visite (cliquez sur « Visiter un bien »).
        Entrez le numéro du bien que vous voulez visiter (par exemple le bien numéro 1) et saississez votre informations (nom, prenom, telephone, mail).
        Vous devenez membre de l'agence (vous avez donc un numéro, retenez-le pour envoyer votre candidature).
        Une date aléatoire de visite en 2019 vous est proposée.
        Tant que vous tapez "false" dans la console, une nouvelle date est générée.
        Une fois que la date vous convient, tapez "true" : vous avez fixé un rendez-vous pour une visite.
    
    4. Envoyer un dossier de candidature (cliquez sur « Envoyer un dossier de candidature »).
        En entrant votre id, l'application affiche le bien que vous avez visité.
        Si vous souhaitez envoyer votre candidature, cliquez sur "Oui" : votre dossier est envoyé.
        Sinon, cliquez sur "Non" : votre parcours s'arrête là, vous pouvez rechercher de nouveaux biens.
    


- Si vous avez cliqué sur Agent : 


    Une fenêtre apparait pour vous identifier.
    Prenons par exemple les identifiants de l'agent ROUX Nicolas id = 4, mot de passe = Nico!!8.
    Cliquez sur "Ok" pour vous connecter.
    Vous pourrez :
    1. Estimer un bien (cliquez sur « Estimer un bien »).
        Entrez votre id (4) et cliquez sur "Voir un bien à estimer".
        Un bien aléatoire de l'agence est proposé dans la console avec ses caractéristiques principales.
        Son prix indicatif (calculé selon sa surface et sa distance aux commodités) est affiché.
        Vous pouvez entrer le prix qui vous semble le plus juste et cliquer sur "Valider l'estimation".
        Le prix du bien est question est alors attribué et envoyé au client.
    
    2. Publier une annonce (cliquez sur « Publier une annonce »).
        Cliquer sur "Voir un bien en attente de publication".
        Un bien de l'agence dont le prix estimé a été accepté par le client est affiché dans la console.
        En relisant bien ses caractéristiques, vous avez le choix de publier ou non l'annonce (bouton "Oui" ou "Non").
    
    3. Fixer la date d'une visite (cliquez sur « Organiser une visite »).
        Vous pouvez choisir un jour, un mois, une année, une heure et des minutes pour fixez la date de votre prochain rendez-vous.
        En choisissant la date, vous lancez une animation graphique symbolisant la visite.
    
    4. Conclure une transaction (cliquez sur « Conclure une transaction »).
        En entrant votre id (4), vous accédez à la dernière transaction que vous venez de conclure.
        Votre note augmente alors de 10% du prix auquel est signé le contrat.



- Si vous avez cliqué sur Responsable : 


    Une fenêtre apparait pour vous identifier.
    Prenons par exemple les identifiants de l'agent BOUSQUET Karine id = 5, mot de passe = Kr/bous1.
    Cliquez sur "Ok" pour vous connecter.
    Vous pourrez :
    1. Valider un compte client (cliquez sur « Valider un compte client »).
    2. Attribuer des primes aux agents (cliquez sur « Attribuer des primes »).
    3. Editer des statistiques sur l'agence (cliquez sur « Editer des statistiques »).



NB : en plus de l'interface, vous pouvez essayer d'executer la classe Main du package 
"agenceImmobiliere" qui permet de tester toutes les méthodes 
des biens et des personnes via des scanners dans la console.



## Auteurs

WENCLIK Laura, STRICKLER Marie, DUPUIS Mélissa (ING 1).